#!/usr/bin/env python
import yaml

# (table, fields, data_fields)

def create_sql(table, fields):
    sql = list()
    sql.append("CREATE TABLE {0} ".format(table))
    sql.append("({0}_id SERIAL PRIMARY KEY".format(table))
    for item, value in fields.items():
        sql.append(', {0}_{1} {2}'.format(table, item, value))
    for elements in data_fields:
        sql.append(', {0} timestampTZ NULL'.format(elements))        
    sql.append(");\n")
    sql.append("\n")
    return "".join(sql)

def create_function(fields):
    funct = list()
    funct.append("CREATE OR REPLACE FUNCTION {0}_data() RETURNS TRIGGER AS $$\n".format(fields))
    funct.append("\tBEGIN\n")
    funct.append("\t\tNEW.{0} = now();\n".format(fields))
    funct.append("\t\tRETURN NEW;\n")
    funct.append("\tEND\n")
    funct.append("\t$$ language 'plpgsql';\n\n")
    return "".join(funct)

def create_trigger(tables, fields, triger_action):
    trigger = list()
    trigger.append("CREATE TRIGGER {0}_data\n".format(fields))
    trigger.append("\tBEFORE {0} ON {1}\n".format(triger_action, tables))
    trigger.append("\tFOR EACH ROW\n".format(fields))
    trigger.append("\tEXECUTE PROCEDURE {0}_data();\n\n".format(fields))
    return "".join(trigger)

def create_altertab(file_data):
    altertab = []
    exist_relations = []
    
    # exist_relations = []
    # print list(file_data.keys())
    # # print list(file_data.values())
    # exist_relations = [ (item, value.values()[1]) for item, value in file_data.items()]
    
    # # print exist_relations[0][0]
    # # print exist_relations[1][0]+ "--"
    # # print exist_relations[2][0]
    # # print len(exist_relations)
    # print exist_relations
    # # # print exist_relations[1][1].values()[0]
    # # print '------'
    # print exist_relations[0][1].keys()[0] + "--"
    # print exist_relations[1][1].keys()[0]
    # print exist_relations[1][1].keys()[1]
    # print exist_relations[2][1].keys()[0]
    
    # print exist_relations
    # # print len(exist_relations.ke)
    # count = 0
    # for items in exist_relations:
    #     count += 1
    # print count
    # print exist_relations[0][1].values()[0]
    # print exist_relations[1][1].values()[0]
    # print exist_relations[1][1].values()[1]
    # print exist_relations[2][1].values()[0]

    for table, value in file_data.items():
        relations = value.values()[1]
        tablename = table.lower()
        table_fields = list()
        for item, value in relations.items(): 
            relation_field = item.lower()
            table_fields.append(tablename)    
            table_fields.append(relation_field)
            table_fields.append(value)
            exist_relations.append(table_fields) 
            table_fields = list()
    for i in range(len(exist_relations)):
        for j in range(len(exist_relations)):
            left_table = exist_relations[i][0]
            left_field = exist_relations[i][1]
            left_value = exist_relations[i][2]
            right_table = exist_relations[j][0]
            right_field = exist_relations[j][1]
            right_value = exist_relations[j][2]   
            if ((left_table == right_field ) and (right_table == left_field) and (left_value == 'one') and (right_value == 'many')):
                print exist_relations[i],exist_relations[j]
                altertab.append("ALTER TABLE {0} ADD {1}_id INTEGER;\n".format(left_table, left_field))
                altertab.append("ALTER TABLE {0} ADD CONSTRAINT {1}_id +FOREIGN KEY ({1}_id) REFERENCES {1} ({1}_id);\n".format(left_table, left_field))
                altertab.append("\n")
            if ((left_table == right_field ) and (right_table == left_field) and (left_value == 'many') and (right_value == 'many')):
                
                print exist_relations[i],exist_relations[j]


    return "".join(altertab)
    
stream = open("in.yaml", 'r')
out = open('out.txt', 'w')
file_data = yaml.load(stream)
data_fields = ['created', 'update']
trigger_action = ['INSERT', 'UPDATE']
for item, value in file_data.items():
    fields = value.values()[0]
    table = item.lower()
    sql_report = create_sql(table, fields)
    out.write(sql_report)
altertab_report = create_altertab(file_data)
out.write(altertab_report)
i = 0 
for i in range(len(data_fields)):
    function_report = create_function(data_fields[i])
    out.write(function_report)
    for item, value in file_data.items():
        tables = item.lower()
        triggers_report = create_trigger(tables, data_fields[i], trigger_action[i])
        out.write(triggers_report)
i += 1
stream.close()
out.close()
