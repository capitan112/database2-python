#!/usr/bin/env python
import yaml

def create_sql(table, lst, fields):
    sql = list()
    sql.append("CREATE TABLE {0} ".format(table))
    sql.append("({0}_id SERIAL PRIMARY KEY".format(table))
    for item, value in lst.items():
        sql.append(', {0}_{1} {2}'.format(table, item, value))
    for elements in fields:
        sql.append(', {0} timestampTZ NULL'.format(elements))        
    sql.append(");\n")
    sql.append("\n")
    return "".join(sql)

def create_function(fields):
    funct = list()
    funct.append("CREATE OR REPLACE FUNCTION {0}_data() RETURNS TRIGGER AS $$\n".format(fields))
    funct.append("\tBEGIN\n")
    funct.append("\t\tNEW.{0} = now();\n".format(fields))
    funct.append("\t\tRETURN NEW;\n")
    funct.append("\tEND\n")
    funct.append("\t$$ language 'plpgsql';\n\n")
    return "".join(funct)

def create_trigger(tables, fields, triger_action):
    trigger = list()
    trigger.append("CREATE TRIGGER {0}_data\n".format(fields))
    trigger.append("\tBEFORE {0} ON {1}\n".format(triger_action, tables))
    trigger.append("\tFOR EACH ROW\n".format(fields))
    trigger.append("\tEXECUTE PROCEDURE {0}_data();\n\n".format(fields))
    return "".join(trigger)
    
stream = open("in.yaml", 'r')
out = open('out.txt', 'w')
file_data = yaml.load(stream)
data_fields = ['created', 'update']
triger_action = ['INSERT', 'UPDATE']
for item, value in file_data.items():
    fields = value.values()[0]
    table = item.lower()
    sql_report = create_sql(table, fields, data_fields)
    out.write(sql_report)   
i = 0 
for i in range(len(data_fields)):
    function_report = create_function(data_fields[i])
    out.write(function_report)
    for item, value in file_data.items():
        tables = item.lower()
        triggers_report = create_trigger(tables, data_fields[i], triger_action[i])
        out.write(triggers_report)
i += 1
stream.close()
out.close()
