CREATE TABLE category (category_id SERIAL PRIMARY KEY, category_title varchar(50), 
	created timestampTZ NULL, update timestampTZ NULL );

-- CREATE TABLE article (article_id SERIAL PRIMARY KEY, article_text text, article_title varchar(50), 
-- 	created timestampTZ NULL, update timestampTZ NULL, category_id integer REFERENCES category);


CREATE TABLE article (article_id SERIAL PRIMARY KEY, article_text text, article_title varchar(50), 
	created timestampTZ NULL, update timestampTZ NULL, category_id INTEGER, FOREIGN KEY (category_id) REFERENCES category(category_id));

-- синтаксис ссылки отношения одного ко многим
-- category_id integer REFERENCES category);

-- синтаксис вставки нового столбца в таблицу
-- ALTER TABLE my_table ADD CONSTRAINT my_fk FOREIGN KEY (my_field) REFERENCES my_foreign_table ON DELETE CASCADE;

CREATE TABLE article (article_id SERIAL PRIMARY KEY, article_text text, article_title varchar(50), created timestampTZ NULL, update timestampTZ NULL);
ALTER TABLE article ADD category_id INTEGER;
ALTER TABLE article ADD CONSTRAINT category_id FOREIGN KEY (category_id) REFERENCES category (category_id);

-- ALTER TABLE article ADD article_category INTEGER;
-- ALTER TABLE article ADD CONSTRAINT category_id FOREIGN KEY (article_category) REFERENCES category (category_id);

-- ALTER TABLE article ADD CONSTRAINT category_id_fk FOREIGN KEY (category_id_fk) REFERENCES category (category_id) ON DELETE CASCADE;

INSERT INTO category (category_title) VALUES ('Foods');
INSERT INTO category (category_title) VALUES ('Water');

UPDATE category
SET category_title = 'Wood'
WHERE category_title = 'Foods';


INSERT INTO article (article_text, article_title, category_id) VALUES ('Pepsi', 'Drinks', 1);
INSERT INTO tag (tag_value) VALUES (5);
select * from category;
select * from article;

select article_text, category_title
from article, category
where article.category_id = category.category_id;

DROP TABLE article;
DROP TABLE category;
DROP TABLE tag;
DROP TABLE article_tag;
DROP FUNCTION created_data();
DROP FUNCTION update_data();


CREATE TABLE article_tag (
    article_id integer REFERENCES article,
    tag_id integer REFERENCES tag,
    PRIMARY KEY (article_id, tag_id),
    created timestampTZ NULL, update timestampTZ NULL);

CREATE TABLE article_tag (
	article_id integer REFERENCES article, 
	tag_id integer REFERENCES tag,  
	PRIMARY KEY (article_id tag_id), 
	created timestampTZ NULL, update timestampTZ NULL);


