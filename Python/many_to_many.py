#!/usr/bin/env python
import yaml

def create_sql(table, fields):
    sql = list()
    sql.append("CREATE TABLE {0} ".format(table))
    sql.append("({0}_id SERIAL PRIMARY KEY".format(table))
    for item, value in fields.items():
        sql.append(', {0}_{1} {2}'.format(table, item, value))
    for elements in data_fields:
        sql.append(', {0} timestampTZ NULL'.format(elements))        
    sql.append(");\n")
    sql.append("\n")
    return "".join(sql)

def create_many_to_many(table, left_field, right_field):
    sql = list()
    sql.append("CREATE TABLE {0} ".format(table))
    sql.append("({0}_id integer REFERENCES {0}, ".format(left_field))
    sql.append("{0}_id integer REFERENCES {0}, ".format(right_field))
    sql.append(" PRIMARY KEY ({0}_id, {1}_id)".format(left_field, right_field))
    for elements in data_fields:
        sql.append(', {0} timestampTZ NULL'.format(elements))        
    sql.append(");\n")
    sql.append("\n")

    return "".join(sql)

def create_function(fields):
    funct = list()
    funct.append("CREATE OR REPLACE FUNCTION {0}_data()" 
                 "RETURNS TRIGGER AS $$\n".format(fields))
    funct.append("\tBEGIN\n")
    funct.append("\t\tNEW.{0} = now();\n".format(fields))
    funct.append("\t\tRETURN NEW;\n")
    funct.append("\tEND\n")
    funct.append("\t$$ language 'plpgsql';\n\n")
    return "".join(funct)

def create_trigger(tables, fields, triger_action):
    trigger = list()
    trigger.append("CREATE TRIGGER {0}_data\n".format(fields))
    trigger.append("\tBEFORE {0} ON {1}\n".format(triger_action, tables))
    trigger.append("\tFOR EACH ROW\n".format(fields))
    trigger.append("\tEXECUTE PROCEDURE {0}_data();\n\n".format(fields))
    return "".join(trigger)

def create_altertab(file_data):
    altertab = []
    exist_relations = []
    for table, value in file_data.items():
        relations = value.values()[1]
        tablename = table.lower()
        table_fields = []
        for item, value in relations.items(): 
            relation_field = item.lower()
            table_fields.append(tablename)    
            table_fields.append(relation_field)
            table_fields.append(value)
            exist_relations.append(table_fields) 
            print table_fields
            table_fields = list()

    
    for i in range(len(exist_relations)):
        for j in range(len(exist_relations)):
            left_table = exist_relations[i][0]
            left_field = exist_relations[i][1]
            left_value = exist_relations[i][2]
            right_table = exist_relations[j][0]
            right_field = exist_relations[j][1]
            right_value = exist_relations[j][2]   
            if ((left_table == right_field ) 
                and (right_table == left_field) 
                and (left_value == 'one') 
                and (right_value == 'many')):
                altertab.append("ALTER TABLE {0} ADD {1}_id INTEGER;\n"
                                 .format(left_table, left_field))
                altertab.append(("ALTER TABLE {0} ADD CONSTRAINT {1}_id " 
                                 "FOREIGN KEY ({1}_id) REFERENCES {1} ({1}_id);\n")
                                 .format(left_table, left_field))
                altertab.append("\n")                       
            if ((left_table == right_field ) and (right_table == left_field) 
                  and (left_value == 'many') and (right_value == 'many')):
                new_table = left_table + '_'+ right_table
                altertab.append(create_many_to_many(new_table, 
                                                    left_table, 
                                                    right_table))
                file_data.update({new_table:None})
                return "".join(altertab)
    return "".join(altertab)

stream = open("in.yaml", 'r')
out = open('out.txt', 'w')
file_data = yaml.load(stream)
data_fields = ['created', 'update']
trigger_action = ['INSERT', 'UPDATE']
for item, value in file_data.items():
    fields = value.values()[0]
    table = item.lower()
    sql_report = create_sql(table, fields)
    out.write(sql_report)
altertab_report = create_altertab(file_data)
out.write(altertab_report)
for i in range(len(data_fields)):
    function_report = create_function(data_fields[i])
    out.write(function_report)
    for item, value in file_data.items():
        tables = item.lower()
        triggers_report = create_trigger(tables, 
        data_fields[i], 
        trigger_action[i])
        out.write(triggers_report)
stream.close()
out.close()
